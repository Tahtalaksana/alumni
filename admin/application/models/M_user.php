<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class M_user extends CI_Model{

	
	public function find_by_id($id){
		$query = $this->db->select('*')->from('tb_user')->where('id',$id)->get();

		if($query->num_rows()==1){
			return $query->row();
		}else{
			return false;
		}
	}

	public function get_old_password($id){
		$query = $this->db->select('password')->from('tb_user')->where('id',$id)->get();

		if($query->num_rows()==1){
			//var_dump($query->row());
			return $query->row()->password;
		}else{
			return false;
		}
	}

	public function find_one($username, $password){
		$this->db->select('*')->from('tb_user')
				->where('username',$username)
				->where('password',md5($password))
				->limit(1);
		$query=$this->db->get();

		if($query->num_rows()==1){
			return $query->row();
		}else{
			return false;
		}
	}

	public function create($data){
		try{
			$this->db->insert('journey', $data);
			return true;
		}catch(Exception $e){
			return $e;
		}
	}

	public function update($id,$data){
		try{
			$this->db->where('id',$id)->update('tb_user', $data);
			return true;
		}catch(Exception $e){
			return $e;
		}
	}

	public function delete($id){
		try {
			$this->db->where('id',$id)->delete('journey');
			return true;
		}catch(Exception $e) {
		  echo $e->getMessage();
		}
	}
}
?>