<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		if($this->session->userdata('logged_in')){
			$this->user_session = $this->session->userdata('logged_in');
		}else{
			redirect(['User_Authentication']);
		}
	}

	public function index()
	{
		//$user_session = $this->check_login_session();
		//var_dump($user_data);
		$data= ['user_session'=>$this->user_session,
				'content'=>'dashboard',
				'title'=>'Dashboard'
		];
		$this->load->view('layout/wrapper',$data);
	}

	public function test(){
		var_dump($this->user_session);
	} 

	/*private function check_login_session(){
		if($this->session->userdata('logged_in')){
			return $this->session->userdata('logged_in');
		}else{
			redirect(['User_Authentication']);
		}
	}*/
}
