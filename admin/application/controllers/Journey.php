<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Journey extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_journey');
		
		if(!$this->session->userdata('logged_in')){
			redirect(['User_Authentication']);
		}

		$this->upload_config =	[
						'upload_path'	=> '../assets/img/journey/',
						'allowed_types' => 'jpeg|jpg|png',
						'max_size'      => 2048,
						'max_width'     => 1920,
						'max_height'    => 1200,
						'encrypt_name'	=> true
		];
		$this->upload->initialize($this->upload_config);

	}

	public function index(){

		$data= [
				'content'	=> 'journey/index',
				'title'		=> 'Journey',
				'journey'	=> $this->m_journey->find_all(),
		];
		$this->load->view('layout/wrapper',$data);
	}

	public function add(){

		$this->form_validation->set_rules('title','Title','required');
		$this->form_validation->set_rules('content','Content','required');
		$this->form_validation->set_rules('status','Status','required');
		
		$this->form_validation->set_error_delimiters('<p class="help-block text-red">','</p>');

		if($this->form_validation->run()){
			
			if ( ! $this->upload->do_upload('thumbnail')){

            	$data=[
                		'content'		=> 'journey/create',
						'title'			=> 'Create Journey',
						'upload_error'	=> $this->upload->display_errors('<p class="help-block text-red">','</p>')
	            ];

            $this->load->view('layout/wrapper', $data);
            
            }else{

               	$uploaded = $this->upload->data();
                $data = [
                			'slug'		=> url_title(set_value('title'), 'dash', TRUE),
                			'thumbnail'	=> $uploaded['file_name'],
                			'title'		=> set_value('title'),
                			'content'	=> $this->input->post('content'),
                			'status'	=> set_value('status'),
                			'date'		=> date('Y-m-d H:i:s'),
                			'author'	=> $this->session->userdata('logged_in')['full_name']
                		];

                $this->m_journey->create($data);
				$this->session->set_flashdata('message','New Journey has been added..');
				redirect('journey');
					
            }

		}else{
			
			$data= [
					'content'	=> 'journey/create',
					'title'		=> 'Create Journey'
			];
			$this->load->view('layout/wrapper',$data); 
		}
	}

	public function edit($id){

		$this->form_validation->set_rules('title','Title','required');
		$this->form_validation->set_rules('content','Content','required');
		$this->form_validation->set_rules('status','Status','required');
		$this->form_validation->set_error_delimiters('<p class="help-block text-red">','</p>');

		$journey = $this->m_journey->find_one($id)->row();
		
		if($this->form_validation->run()){

			$update_data = [
    			'slug'		=> url_title(set_value('title'), 'dash', TRUE),
    			'title'		=> set_value('title'),
    			'content'	=> $this->input->post('content'),
    			'status'	=> set_value('status'),
    			'author'	=> $this->session->userdata('logged_in')['full_name']
    		];

			if($_FILES["thumbnail"]["name"]){

	            if ( ! $this->upload->do_upload('thumbnail')){

                    $data=[
                    		'content'		=> 'journey/update',
							'title'			=> 'Edit Journey',
							'journey'		=> $journey,
							'upload_error'	=> $this->upload->display_errors('<p class="help-block text-red">','</p>')
                    ];
                	$this->load->view('layout/wrapper', $data);

	            }else{

	            	$uploaded = $this->upload->data();

					unlink('../assets/img/journey/'.$journey->thumbnail);
                    $update_data['thumbnail'] = $uploaded['file_name'];

	            }

			}
			
			$this->m_journey->update($id,$update_data);
            $this->session->set_flashdata('message','Journey has been updated..');
            redirect('journey');

		}else{

			$data = [
						'content'	=> 'journey/update',
						'title'		=> 'Edit Journey',
						'journey'	=> $journey
			];
			$this->load->view('layout/wrapper',$data);           
		}
	}


	public function delete($id){

		$data = $this->m_journey->find_one($id)->row();
		unlink('../assets/img/journey/'.$data->thumbnail);
		$this->m_journey->delete($id);
		$this->session->set_flashdata('message','One journey has been deleted..');
		redirect('journey');
	} 

}
