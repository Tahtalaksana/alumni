<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Authentication extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('m_user');

		
	}

	public function index(){
		if($this->session->userdata('logged_in')){
			redirect(base_url('dashboard'));
		}
		$rules=	[
		 	[
				'field'=>'username',
		 		'label'=>'Username',
		 		'rules' =>'required'
		 	],
		 	[
				'field'=>'password',
		 		'label'=>'Password',
		 		'rules' =>'required'
		 	]
		];
		$this->form_validation->set_rules($rules);
		
		if($this->form_validation->run()==false){
			$this->load->view('user/login_form');
		}else{/*
			echo set_value('username');//$this->input->post('username');
			echo $this->input->post('password');
			die();*/
				$username 	= $this->input->post('username');
				$password 	= $this->input->post('password');
			$user = $this->m_user->find_one($username, $password);
			if($user){
				$userArray=[
					'id'=>$user->id,
					'username'=>$user->username,
					'full_name'=>$user->full_name,
					'photo'=>$user->photo
				];

				$this->session->set_userdata('logged_in',$userArray);
				redirect(['dashboard']);
			}else{
				$this->load->view('user/login_form',['error'=>'Username and Password Not Match']);
			}
		}		



	}

	public function change_password(){
		if(!$this->session->userdata('logged_in')){
			redirect($this);
		}

		$this->form_validation->set_rules('old_password','Old Password','trim|required|callback_oldpassword_check');
		$this->form_validation->set_rules('new_password','New Password','trim|required');
		$this->form_validation->set_rules('confirm_password','Confirm Password','trim|required|matches[new_password]');
		$this->form_validation->set_error_delimiters('<p class="help-block text-red">','</p>');

		$id = $this->session->userdata('logged_in')['id'];

		$user = $this->m_user->find_by_id($id);
		
		if($this->form_validation->run()){
			
			$data=[
				'password'=>md5($this->input->post('new_password'))
			];

			$this->m_user->update($id,$data);
			$this->session->set_flashdata('message','Your password has been changed into '.$this->input->post('new_password'));
			redirect('dashboard');


			//redirect(['dashboard']);

		}else{
			$data= [
				'content'	=> 'user/change_password',
				'title'		=> 'Change Password',
				'user'		=> $user
		];

		$this->load->view('layout/wrapper',$data);
		}

	}

	public function oldpassword_check($old_password){
	   $old_password_hash = md5($old_password);

	   $old_password_db_hash = $this->m_user->get_old_password($this->session->userdata('logged_in')['id']);

	   if($old_password_hash != $old_password_db_hash)
	   {
	      $this->form_validation->set_message('oldpassword_check', 'Old password not match');
	      return FALSE;
	   } 
	   return TRUE;
	}


	public function logout(){
		if(!$this->session->userdata('logged_in')){
			redirect($this);
		}

		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect($this);
	}

	public function edit_profile(){
		
		if(!$this->session->userdata('logged_in')){
			redirect($this);
		}
		$this->form_validation->set_rules('username','Username','trim|required');
		$this->form_validation->set_rules('fullname','Fullname','trim|required');

		$user = $this->m_user->find_by_id($user_session['id']);
		
		if($this->form_validation->run()){

		}else{

			$data= [
				'content'	=> 'user/edit_profile',
				'title'		=> 'Edit Profile',
				'user'		=> $user
			];

			$this->load->view('layout/wrapper',$data);
		
		}

	}


	
}


	
 ?>