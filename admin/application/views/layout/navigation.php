<header class="main-header">
  <a href="<?=base_url('../')?>" class="logo">
    <!-- LOGO -->
    <b>Bromo</b>Travel
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">

  	<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
  <span class="sr-only">Toggle navigation</span>
</a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        <li>
          <a href="<?=base_url('../')?>" target="_blank">
            <i class="fa fa-home"></i> Visit Site
          </a>
        </li>
        <!-- <li class="dropdown messages-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-envelope-o"></i>
            <span class="label label-success">4</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">You have 4 messages</li>
            <li>
              inner menu: contains the actual data
              <ul class="menu">
                <li>start message
                  <a href="#">
                    <div class="pull-left">
                      <img src="<?php echo base_url().'assets/img/my_avatar.jpg'; ?>" class="img-circle" alt="User Image">
                    </div>
                    <h4>
                      Sender Name
                      <small><i class="fa fa-clock-o"></i> 5 mins</small>
                    </h4>
                    <p>Message Excerpt</p>
                  </a>
                </li>end message
                ...
              </ul>
            </li>
            <li class="footer"><a href="#">See All Messages</a></li>
          </ul>
        </li> -->
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?= base_url('../assets/img/users/'.$this->session->userdata('logged_in')['photo']); ?>" class="user-image" alt="User Image">
            <span class="hidden-xs"><?= $this->session->userdata('logged_in')['full_name'] ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="<?= base_url('../assets/img/users/'.$this->session->userdata('logged_in')['photo']); ?>" class="img-circle" alt="User Image">
              <p>
                <?= $this->session->userdata('logged_in')['full_name'] ?>
                <small>Administrator Web</small>
              </p>
            </li>
            <!-- Menu Body -->
            <!-- <li class="user-body">
              <div class="col-xs-4 text-center">
                <a href="#">Followers</a>
              </div>
              <div class="col-xs-4 text-center">
                <a href="#">Sales</a>
              </div>
              <div class="col-xs-4 text-center">
                <a href="#">Friends</a>
              </div>
            </li> -->
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="<?= base_url('User_Authentication/change_password') ?>" class="btn btn-default btn-flat">Change Password</a>
              </div>
              <div class="pull-right">
                <a href="<?= base_url('User_Authentication/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>