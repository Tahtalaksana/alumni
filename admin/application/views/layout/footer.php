<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Beta Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; <?= date('Y')?> Bromo Tour And Travel.</strong> All rights reserved.
      </footer>
</div><!-- wrapper -->
<script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
<script src="<?= base_url('assets/adminlte/js/app.min.js');?>"></script>
<script src="<?= base_url('assets/tinymce/tinymce.min.js') ?>"></script>
<script src="<?= base_url('assets/tinymce/tinymce.config.js') ?>"></script>

</body>
</html>