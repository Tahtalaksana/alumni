
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <div class="sidebar" id="scrollspy">

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="nav sidebar-menu">
      <li class="header">SIDEBAR NAVIGATION</li>
      <li class="<?=$this->uri->segment(1)=='dashboard'?'active':''?>">
        <a href="<?= base_url()?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a>
      </li>
      <li class="<?=$this->uri->segment(1)=='slideshow'?'active':''?>">
        <a href="<?= base_url()?>slideshow"><i class="fa fa-sliders"></i> Slideshow</a>
      </li>
      <li class="<?=$this->uri->segment(1)=='journey'?'active':''?>">
        <a href="<?= base_url()?>bekerja"><i class="fa fa-book"></i> Bekerja</a>
      </li>
      <li class="<?=$this->uri->segment(1)=='testimonial'?'active':''?>">
        <a href="<?= base_url()?>testimonial"><i class="fa fa-comment"></i> Testimonial</a>
      </li>
      <li class="<?=$this->uri->segment(1)=='User_Authentication'?'active':''?>">
        <a href="<?= base_url()?>User_Authentication/change_password"><i class="fa fa-user"></i>Change Password</a>
      </li>

      <!-- <li class="treeview <?=$this->uri->segment(1)=='user_authentication'?'active':''?>">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>User Setting</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu menu-open" style="display: none;">
                <li><a href="<?= base_url('user_authentication/edit_profile')?>"><i class="fa fa-circle-o"></i> Edit Profil</a></li>
                <li><a href="<?= base_url('user_authentication/change_password')?>"><i class="fa fa-circle-o"></i> Change Password</a></li>
              </ul>
            </li> -->
    </ul>
  </div>
  <!-- /.sidebar -->
</aside>

