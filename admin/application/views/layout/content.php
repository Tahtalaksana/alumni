<?php 
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <h1 >
      
      <!-- <small>Current version 2.3.0</small> -->
    </h1>
    <!-- <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active"><?= $title ?></li>
    </ol> -->
  </div>

  <!-- Main content -->
  
  <?php 
  if($content){
    $this->load->view($content);
  }
  ?>

</div>
