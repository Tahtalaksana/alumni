<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $title ?></title>
	<link href="<?= base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url(); ?>assets/adminlte/css/adminLTE.min.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url(); ?>assets/adminlte/css/skins/_all-skins.css" rel="stylesheet" type="text/css">
	<!-- <script src="<?= base_url(); ?>assets/js/jquery-v1.11.1.js"></script> -->
	<script src="<?= base_url(); ?>assets/js/jquery-2.2.3.js"></script>
	<style>
		@media (min-width: 768px){
		.form-horizontal .control-label {
		    padding-top: 7px;
		    margin-bottom: 0;
		    text-align: left;
		}
		}
	</style>
	<!-- <script src="<?= base_url(); ?>assets/tinymce/tinymce.min.js"></script>
	<script>
	tinymce.init({
		selector: '#tinymce-editor'
		plugins:[
					'advlist autolink link image lists charmap print preview hr anchor pagebreak',
					'searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking',
					'table contextmenu directionality emoticons paste textcolor responsivefilemanager code'
		],
		toolbar1:'undo rendo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
		toolbar2:'responsivefilemanager | link unlink anchor | image media | forecolor backcolor | print preview code',
		image_advtab:true,
		external_filemanager_path: "<?= base_url().'filemanager/' ?>",
		filemanager_title :"responsife file manager",
		external_plugins:{"filemanager":"<?= base_url().'filemanager.plugin.min.js' ?>"}
	});
	</script> -->

	
</head>

<body class="skin-blue fixed">
	<div class="wrapper">

