<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content">
<div class="row">
	<div class="col-md-12">

        <?php if ($this->session->flashdata('message')):?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p><?=$this->session->flashdata('message')?></p>
          </div>
        <?php endif; ?>

		<div class="box">
            <div class="box-header">
                <h3 class="box-title"><?=$title?></h3>
                <div class="box-tools">
                    <a class="btn btn-primary btn-sm btn-flat" href="<?= base_url('journey/add'); ?>"><span class="glyphicon glyphicon-plus"></span> Add New</a>
                    <!-- <div class="input-group" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div> -->
                </div>
            </div><!-- /.box-header -->
            
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                          <th>Title</th>
                          <th>Author</th>
                          <th>Date</th>
                          <th width="75px">Status</th>
                          <th width="75px" >Action</th>
                        </tr>
                    </thead>
                    <tbody>

					<?php if($journey->num_rows() > 0) :?>
                        <?php foreach ($journey->result() as $data) : ?>
                            <tr>
                              <td><?= $data->title ?></td>
                              <td><?= $data->author ?></td>
                              <td><?= $data->date ?></td>
                              
                              <td>
                                <span class="label <?= $data->status==1 ? 'label-success':'label-default'?>"><?= $data->status==1 ? 'Publish':'Draft'?></span>
                              </td>
                              <td>
                                <?=anchor(base_url().'journey/edit/'.$data->id,'<span class="glyphicon glyphicon-pencil"></span>',['class'=>'label label-primary', 'role'=>'button'])?>
                                <?=anchor(base_url().'journey/delete/'.$data->id,'<span class="glyphicon glyphicon-trash"></span>',['class'=>'label label-primary', 'role'=>'button'])?>
                              </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
						<tr><td colspan='5'> Data Not Found! </td></tr>
                    <?php endif; ?>

                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div>
	</div>
</div>
</section>