<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
	            <div class="box-header">
	              <h3 class="box-title">Add Journey</h3>
	            </div><!-- /.box-header -->
                <div class="box-body">
                    <?=form_open_multipart('journey/add')?>
                    
                    <div class="form-group">
                        <label for="thumbnail" class="control-label">Thumbnail</label>
                        <div class="input-group">
                            <div class="input-group-btn">
                                <span class="btn btn-primary btn-flat btn-file">
                                    <i class="fa fa-fw fa-folder-open"></i> Browse
                                    <input type="file" name="thumbnail" id="thumbnail" accept="image/*" onchange="$('#imgFileInfo').val($(this).val())">
                                </span>
                            </div>
                            <input type="text" for="" id="imgFileInfo" disabled class="form-control" placeholder="Choose image" style="background-color:#ffffff">
                        </div>
                        <p class="help-block text-red"><?=@$upload_error?></p>
                    </div>

                    <div class="form-group">
                        <label for="title" class="control-label">Title</label>
                        <input type="text" name="title" class="form-control" placeholder="Input title" value="<?= set_value('title') ?>">
                        <?=form_error('title')?>
                    </div>
                    
                    <div class="form-group">
                      <label for="content" class="control-label">Content</label>
                        <textarea class="form-control tinymce-editor" id="text-editor" name="content" rows="10" placeholder="Input content...." ><?=set_value('content')?></textarea>
                        <?=form_error('content')?>
                    </div>
                    
                    <div class="form-group">
                      <label for="status" class="control-label">Status</label>
                        <select class="form-control" name="status">
                            <option value="1" <?= set_value('status')==1?'selected':'' ?> >Publish</option>
                            <option value="0" <?= set_value('status')==0?'selected':'' ?>>Draft</option>
						</select>
                        <?=form_error('status')?>
                    </div>
                    
                    <button type="submit" class="btn btn-primary btn-flat">Save</button>
             
                    <?=form_close()?>
                </div><!-- /.box-body -->
                  
	       </div>
		</div>
	</div>
</section>
<script src="<?= base_url('assets/ckeditor/ckeditor.js'); ?>"></script>
<script>

    /*CKEDITOR.replace('text-editor' ,{
    filebrowserImageBrowseUrl : '<?php echo base_url('assets/filemanager/index.html');?>'
    });*/
</script>