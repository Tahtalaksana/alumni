<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
	            <div class="box-header">
	              <h3 class="box-title">Edit Journey</h3>
	            </div><!-- /.box-header -->
                <div class="box-body">
                    
                    <?=form_open_multipart('journey/edit/'.$journey->id)?>
                    
                    <div class="form-group">
                        <label for="thumbnail" class="control-label">Thumbnail</label>
                        <div class="input-group">
                            <div class="input-group-btn">
                                <span class="btn btn-primary btn-flat btn-file">
                                    <i class="fa fa-fw fa-folder-open"></i> Browse
                                    <input type="file" name="thumbnail" id="thumbnail" accept="image/*" onchange="$('#imgFileInfo').val($(this).val())">
                                </span>
                            </div>
                            <input type="text" for="" id="imgFileInfo" disabled class="form-control" placeholder="Choose image" style="background-color:#ffffff">
                        </div>
                        <p class="help-block text-red"><?=@$upload_error?></p>
                    </div>

                    <div class="form-group">
                        <label for="title" class="control-label">Title</label>
                        <input type="text" name="title" class="form-control" placeholder="Input title" value="<?= @$journey->title ?>">
                        <?=form_error('title')?>
                    </div>
                    
                    <div class="form-group">
                      <label for="content" class="control-label">Content</label>
                        <textarea class="form-control tinymce-editor" id="text-editor" name="content" rows="10" placeholder="Input content...." ><?=@$journey->content?></textarea>
                        <?=form_error('content')?>
                    </div>
                    
                    <div class="form-group">
                      <label for="status" class="control-label">Status</label>
                        <select class="form-control" name="status">
                            <option value="1" <?= $journey->status==1?'selected':'' ?> >Publish</option>
                            <option value="0" <?= $journey->status==0?'selected':'' ?>>Draft</option>
                        </select>
                        <?=form_error('status')?>
                    </div>
                    
                    <button type="submit" class="btn btn-primary btn-flat">Update</button>
             
                    <?=form_close()?>
               
                </div>
	       </div>
		</div>
	</div>
</section>

<script src="<?= base_url('assets/ckeditor/ckeditor.js'); ?>"></script>
<script>
/*CKEDITOR.replace('text-editor' ,{
    filebrowserImageBrowseUrl : '<?php echo base_url('assets/filemanager/index.html');?>'
    });*/

$('.btn-file :file').on('fileselect', function(event, numFiles, label) {   
      var input = $(this).parents('.input-group').find(':text'),
          log = numFiles > 1 ? numFiles + ' files selected' : label;
      if( input.length ) {
          input.val(log);
      } else {
          if( log ) alert(log);
      }
  });

	$(document).on('change', '.btn-file :file', function() {


  var extAllowed = ['jpg','jpeg','png'];
  var myFile = $(this).val();
  if(myFile){

    var myExt = myFile.split('.');
    myExt=myExt.reverse();

    if($.inArray(myExt[0].toLowerCase(), extAllowed)>-1){
      //return true;
      var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    }else{
      alert("Gambar Harus Bertipe .jpg, .jpeg, atau .png");
      
      var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = "";
      input.trigger('fileselect', [numFiles, label]);
      $(this).val("");
    }

  }
  
});
</script>
