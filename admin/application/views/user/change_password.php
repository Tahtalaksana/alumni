<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
	            <div class="box-header">
	              <h3 class="box-title"><?= @$title ?></h3>
	            </div><!-- /.box-header -->
	            <div class="box-body">
	              
                  <?=form_open_multipart()?>
                  
                    <div class="form-group">
                        <label for="old_password" class="control-label">Old Password</label>
                        <input type="password" name="old_password" class="form-control" placeholder="Input old password" value="<?=@set_value('old_password') ?>">
                        <?=form_error('old_password')?>
                    </div>

                    <div class="form-group">
                        <label for="new_password" class="control-label">New Password</label>
                        <input type="password" name="new_password" class="form-control" placeholder="Input new password" value="<?=@set_value('new_password') ?>">
                        <?=form_error('new_password')?>
                    </div>

                    <div class="form-group">
                        <label for="confirm_password" class="control-label">Confirm Password</label>
                        <input type="password" name="confirm_password" class="form-control" placeholder="Input confirm password" value="<?=@set_value('confirm_password') ?>">
                        <?=form_error('confirm_password')?>
                    </div>

                    
                    <button type="submit" class="btn btn-primary">Update</button></div>
                    
                  </div><!-- /.box-body -->
                  
                </form>
		</div>
	</div>
</section>

