<div class="container">
	<div class="row">
		<div class="col-md-8">
		<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
      <form class="form-horizontal" action="<?php echo base_url('bekerja/simpan_data');?>" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="inputEmail3">nama kerjaan</label>

                  <div class="col-sm-10">
                    <input type="text" name="nama_kerja" placeholder="Kerjaanmu sekarang" id="inputEmail3" class="form-control">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="inputPassword3">jenis kerjaan</label>

                  <div class="col-sm-10">
                    <input type="text" name="jenis_pekerjaan" placeholder="Instansi tempat bekerja" id="inputPassword3" class="form-control">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button class="btn btn-default" type="submit">Cancel</button>
                <button class="btn btn-info pull-right" type="submit">Tambah</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
       </div>
	</div>

</div>