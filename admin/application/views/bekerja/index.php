<div class="container">
	<div class="row">
	<div class="col-md-8">
		<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Bordered Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           	  <a href="<?php echo base_url('bekerja/tambah')?>"><button class="btn btn-primary" type="button"><i class="fa fa-fw fa-plus-circle"></i> Tambah Data</button></a> 
              <table class="table table-bordered">
                <tbody>
                <tr>
                  <th style="width: 10px">No</th>
                  <th>kerja</th>
                  <th>jenis</th>
                </tr>

                <?php foreach ($pekerja as $value) :?>
                	
               

                <tr>
                  <td><?= $value->id_bekerja ?></td>
                  <td><?= $value->nama_pekerjaan ?></td>
                  <td><?= $value->jenis_kerja ?></td>
                </tr>
 				<?php endforeach; ?>


              </tbody></table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">»</a></li>
              </ul>
            </div>
          </div>
	</div>
</div>
</div>