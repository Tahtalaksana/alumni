<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content">
<div class="row">
	<div class="col-md-12">
		<div class="box">
            <div class="box-header">
                <h3 class="box-title"><?=$title?></h3>
                <!-- <div class="box-tools">
                    
                </div> -->
            </div><!-- /.box-header -->
            
            <?php if ($this->session->flashdata('message')):?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p><?=$this->session->flashdata('message')?></p>
          </div>
        <?php endif; ?>

            <div class="box-body">
                <p>Wellcome To Administrator Page</p>
            </div><!-- /.box-body -->
        </div>
	</div>
</div>
</section>