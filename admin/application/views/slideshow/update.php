<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
	            <div class="box-header">
	              <h3 class="box-title">Edit Slideshow</h3>
	            </div><!-- /.box-header -->
	            <div class="box-body">
	              
                  <?=form_open_multipart(base_url('slideshow/edit/'.$slideshow->id))?>
                  
                  	<div class="form-group">
                        <label for="img" class="control-label">Image</label>
                        <div class="input-group">
                            <div class="input-group-btn">
                                <span class="btn btn-primary btn-flat btn-file">
                                    <i class="fa fa-fw fa-folder-open"></i> Browse
                                    <input type="file" name="img" id="img" accept="image/*" onchange="$('#imgFileInfo').val($(this).val())">
                                </span>
                            </div>
                            <input type="text" for="" id="imgFileInfo" disabled class="form-control" placeholder="Choose image" style="background-color:#ffffff">
                        </div>
                        <p class="help-block text-red"><?=@$upload_error?></p>
                    </div>

                    <div class="form-group">
                        <label for="caption" class="control-label">Caption</label>
                        <input type="text" name="caption" class="form-control" placeholder="Input caption" value="<?= $slideshow->caption ?>">
                        <?=form_error('caption')?>
                    </div>


                    <div class="form-group">
                        <label for="description" class="control-label">Description</label>
                        <textarea class="form-control" name="description" rows="3" placeholder="Input desription"><?= $slideshow->description ?></textarea>
                        <?=form_error('description')?>
                    </div>

                    <div class="form-group">
                        <label for="status" class="control-label">Status</label>
                        <select class="form-control" name="status">
                            <option value="1" <?= $slideshow->status==1?'selected':'' ?> >Show</option>
                            <option value="0" <?= $slideshow->status==0?'selected':'' ?>>Hide</option>
                        </select>
                        <?=form_error('status')?>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Update</button></div>
                    
                  </div><!-- /.box-body -->
                  
                </form>
		</div>
	</div>
</section>

