<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add New Slideshow</h3>
                </div><!-- /.box-header -->
               <form action="<?=base_url('slideshow/save')?>" id="slideshow-form" class="form-horizontal" enctype="multipart/form-data" method="post" accept-charset="utf-8">
               
               <!-- <?=form_open_multipart(base_url('slideshow/save'),['id'=>'slideshow-form','class'=>'form-horizontal']); ?> -->

                <input type="hidden" name="csrf_test_name" value="<?=$this->security->get_csrf_hash()?>" style="display:none;">

                    
                    <div class="box-body">
                        <?php if(validation_errors() || isset($error)) : ?>
                            <div class="alert alert-warning alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                                <?=validation_errors()?>
                                <?=(isset($error)?$error:'')?>
                            </div>
                        <?php endif; ?>
        
                        <div id="msgBox"></div>

                        <div class="form-group">
                            <label for="img" class="col-md-2 control-label">Image</label>
                            <div class="col-md-10">
                                <!-- <input type="file" id="img" class="form-control" name="img"> -->
    

                                <div class="input-group" id="imgInputGroup">
                                    <div class="input-group-btn">
                                        <span class="btn btn-default btn-flat btn-file">
                                        <i class="fa fa-fw fa-folder-open"></i>
                                        <input type="file" name="img" id="img" accept="image/*" onchange="$('#imgFileInfo').val($(this).val())">
                                        </span>
                                    </div>
                                    <input type="text" id="imgFileInfo"  class="form-control" onkeypress="return false;" placeholder="img">
                                </div>
                          </div>
                        </div>


                        <div class="form-group">
                            <label for="caption" class="col-md-2 control-label">Caption</label>
                            <div class="col-md-10">
                            <input type="text" name="caption" id="caption" class="form-control" placeholder="Caption">                       
                            </div>
                        </div>  

                        <div class="form-group">
                            <label for="description" class="col-md-2 control-label">Description</label>
                            <div class="col-md-10">
                                <textarea class="form-control" name="description" id="description" rows="3" placeholder="Desription ..."></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                          <label for="status" class="col-md-2 control-label">Status</label>
                          <div class="col-md-10">
                            <select class="form-control" name="status" id="status">
                                <option value="1">Show</option>
                                <option value="0">Hide</option>
                            </select>
                        </div>
                        </div>
                        <div class="form-group">
                            
                    <div class="col-md-10 col-md-offset-2">
                        
                            <button type="submit" class="btn btn-success">Save</button>
                    </div> 
                        </div>
                    </div><!-- /.box-body -->
                <?=form_close();?>
              </div><!-- BOX -->
        </div>
    </div>
</section>



<script>
    $('#slideshow-form').submit(function(e){
        e.preventDefault();

        var me=$(this);
        var data = new FormData(me[0]);
        $.ajax({
            url:me.attr('action'),
            type:'post',
            data:data,
            mimeType:'multipart/form-data',
         /*   async:false,
            cache:false,*/
            contentType:false,
            processData:false,
            dataType:'json',
            success : function(response){
               // alert(response);
                console.log(response);
                if(response.success==true){
                    me[0].reset();
                    console.log('success');
                    $('#msgBox').append('<div class="alert alert-success">' +
                                            '<span class="glyphicon glyphicon-ok"></span>' +
                                            ' Data has been saved' +
                                        '</div>'
                    );
                    $('.form-group').removeClass('has-error').removeClass('has-success');
                    $('.text-danger').remove();
                    
                    $('.alert-success').delay(500).show(10, function() {
                        $(this).delay(3000).hide(10, function() {
                            $(this).remove();
                            location.href = '<?=base_url("slideshow")?>';
                        });
                    })
                }else{
                   console.log('error');
                    $.each(response.msg, function(key, value){
                        console.log(key+ ' '+value+'\n');
                        var element = $('#'+key);
                        element.closest('div.form-group')
                            .removeClass('has-error')
                            .addClass(value.length>0 ? 'has-error':'has-success')
                            .find('.text-danger').remove();
                        element.after(value);
                    });
                }
            },
            error:function(err){
                console.log(err);
            }
        });
    });
    
</script>