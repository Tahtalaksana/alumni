<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content">
<div class="row">
	<div class="col-md-12">
		<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Slideshow</h3>
                  <div class="box-tools">
                  	<a class="btn btn-primary btn-sm" href="<?= base_url(); ?>slideshow/add"><span class="glyphicon glyphicon-plus"></span> Add New</a>
                    <!-- <div class="input-group" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div> -->
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tbody><tr>
                      <!-- <th width="85px">ID</th> -->
                      <th>Title</th>
                      <th>Description</th>
                      <th width="50px">Status</th>
                      <th width="75px">Action</th>
                    </tr>
					<?php if($slideshow->num_rows() > 0) : ?>
						<?php foreach ($slideshow->result() as $data): ?>
							<tr>
								<!-- <td><?= $data->id ?></td> -->
								<td><?= $data->caption ?></td>
								<td><?=substr($data->description, 0,100)?>...</td>
								<td>
									<span class="label <?php echo $data->status==1 ? 'label-success':'label-default'?>">
										<?php echo $data->status==1 ? 'Show':'Hide'?>
									</span>
								</td>
								<td>
									<!-- <a href="#" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a> -->
									<?=anchor(base_url().'slideshow/edit/'.$data->id,'<span class="glyphicon glyphicon-pencil">',['class'=>'label label-warning'])?>
									<?=anchor(base_url().'slideshow/delete/'.$data->id,'<span class="glyphicon glyphicon-trash">',['class'=>'label label-danger'])?>
									<!-- <a href="<?= base_url()?>slideshow/edit/<?= $data->id ?>"><span class="glyphicon glyphicon-pencil"></span></a>
									<a href="<?= base_url()?>slideshow/delete/<?= $data->id ?>"><span class="glyphicon glyphicon-trash"></span></a> -->
								</td>		
		                    </tr>
						<?php endforeach; ?>
					<?php else : ?>
						<tr><td colspan='5'> Data Not Found! </td></tr>
					<?php endif; ?>
					
                  </tbody>
                </table>
                </div><!-- /.box-body -->
              </div>
	</div>
</div>
</section>