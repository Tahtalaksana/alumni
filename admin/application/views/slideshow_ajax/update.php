<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
	            <div class="box-header">
	              <h3 class="box-title">Edit Slideshow</h3>
	            </div><!-- /.box-header -->
	            <div class="box-body">
	              <form id="slideshow-form" class="form-horizontal" method="POST" action="<?= base_url();?>slideshow/edit/<?= $slideshow->id ?>" enctype="multipart/form-data">
                  <div class="box-body">
					<?php if(validation_errors() || isset($error)) : ?>
                            <div class="alert alert-warning alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                                <?=validation_errors()?>
                                <?=(isset($error)?$error:'')?>
                            </div>
                        <?php endif; ?>
					<div class="form-group">
                      <label for="img" class="col-sm-2 control-label text-left">Image</label>
                      <div class="col-sm-10">
                        <div class="input-group">
					  	<div class="input-group-btn">
					  		<span class="btn btn-default btn-flat btn-file">
					  			<i class="fa fa-fw fa-folder-open"></i>
									<input type="file" name="img" accept="image/*">
					  		</span>
					  	</div>
					  	<input type="text" class="form-control" readonly="" placeholder="foto">
					  </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="caption" class="col-sm-2 control-label">Caption</label>
                      <div class="col-sm-10">
                        <input type="text" name="caption" class="form-control" placeholder="Caption" value="<?= $slideshow->caption ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="description" class="col-sm-2 control-label">Description</label>
                      <div class="col-sm-10">
                        <textarea class="form-control" name="description" rows="3" placeholder="Enter ..."><?= $slideshow->description ?></textarea>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Status</label>
                      <div class="col-sm-10">
                        <select class="form-control" name="status">
						                 <option value="1" <?php if($slideshow->status==1) echo 'selected';?> >Show</option>
            						    <option value="0" <?php if($slideshow->status==0) echo 'selected';?>>Hide</option>
            						</select>
                      </div>
                    </div>
                    <div class="form-group">
                      <input type="hidden" id="slide-id" value="<?= $slideshow->id ?>">
                  		<div class="col-md-10 col-md-offset-2"><button type="submit" class="btn btn-success">Update</button></div>
                    </div>

                  </div><!-- /.box-body -->
                  
                </form>
		</div>
	</div>
</section>

<script>
  
</script>