<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | 404 Page not found</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
   
   <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/adminlte/css/adminLTE.min.css" rel="stylesheet" type="text/css">
	<link href="../assets/adminlte/css/skins/_all-skins.css" rel="stylesheet" type="text/css">
	<script src="../assets/js/jquery-2.2.3.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue" style="background-color:#ecf0f5">


    <div class="container">


        <!-- Main content -->
        <section class="content" style="margin-top:10%">
          <div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>
            <div class="error-content">
              <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
              <p>
                We could not find the page you were looking for.
                Meanwhile, you may <a href="../../">return to dashboard</a> or try using the search form.
              </p>
              <!-- <form class="search-form">
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>
                  </div>
                </div>/.input-group
              </form> -->
            </div><!-- /.error-content -->
          </div><!-- /.error-page -->
        </section><!-- /.content -->
</div>

<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/adminlte/js/app.min.js"></script>
    <!-- jQuery 2.1.4
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    Bootstrap 3.3.5
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    FastClick
    <script src="../../plugins/fastclick/fastclick.min.js"></script>
    AdminLTE App
    <script src="../../dist/js/app.min.js"></script>
    AdminLTE for demo purposes
    <script src="../../dist/js/demo.js"></script> -->
  </body>
</html>
