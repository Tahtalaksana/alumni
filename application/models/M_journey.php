<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class M_journey extends CI_Model{

	public function search($data){
		$query = $this->db->select('*')->from('journey')
		->where('status',1)->where('title LIKE "%'.$data.'%"')->get();
		return $query;
	}

	public function detail($slug){
		$row = $this->db->select('*')->from('journey')
		->where('slug',$slug)
		->where('status',1)->get();
		return $row;
	}

	public function view($num,$offset){
		$result = $this->db->where('status',1)->get('journey', $num,$offset);
		return $result;
	}

	public function recent_journey(){
		$result = $this->db->select('*')->from('journey')->where('status',1)->order_by('id','desc')->limit(6)->get();
		return $result;
	}

	public function find_all(){
		$result = $this->db->where('status',1)->get('journey');
		return $result;
	}

	public function find_one($id){
		$row = $this->db->select('*')->from('journey')->where('id',$id)->get();
		return $row;
	}

	public function create($data){
		try{
			$this->db->insert('journey', $data);
			return true;
		}catch(Exception $e){
			return $e;
		}
	}

	public function update($id,$data){
		try{
			$this->db->where('id',$id)->update('journey', $data);
			return true;
		}catch(Exception $e){
			return $e;
		}
	}

	public function delete($id){
		try {
			$this->db->where('id',$id)->delete('journey');
			return true;
		}catch(Exception $e) {
		  echo $e->getMessage();
		}
	}
}
?>