<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class M_testimonial extends CI_Model{

	public function find_all(){
		$result = $this->db->where('status','approve')->get('testimonials');
		return $result;
	}

	public function create($data){
		try{
			$this->db->insert('testimonials', $data);
			return true;
		}catch(Exception $e){
			return $e;
		}
	}

	public function update($id,$data){
		try{
			$this->db->where('id',$id)->update('journey', $data);
			return true;
		}catch(Exception $e){
			return $e;
		}
	}

	public function delete($id){
		try {
			$this->db->where('id',$id)->delete('journey');
			return true;
		}catch(Exception $e) {
		  echo $e->getMessage();
		}
	}
}
?>