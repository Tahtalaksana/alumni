<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class M_slideshow extends CI_Model{

	public function find_all(){
		$result = $this->db->select('*')
					->from('slideshow')->where('status','1')->get();
		return $result;
	}

	public function find_one($id){
		$row = $this->db->select('*')->from('slideshow')->where('id',$id)->get();
		return $row;
	}

	public function create($data){
		try{
			$this->db->insert('slideshow', $data);
			return true;
		}catch(Exception $e){
			return $e;
		}
	}

	public function update($id,$data){
		try{
			$this->db->where('id',$id)->update('slideshow', $data);
			return true;
		}catch(Exception $e){
			return $e;
		}
	}

	public function delete($id){
		try {
			$this->db->where('id',$id)->delete('slideshow');
			return true;
		}catch(Exception $e) {
		  echo $e->getMessage();
		}
	}

	public function generateId(){
		$result = $this->db->query("SELECT id FROM slideshow WHERE id IN (SELECT MAX(id) FROM slideshow)");
		    if($result->num_rows()!=0){
            	$last_id = $result->row()->id;
                $max =  substr($last_id, 7);
                $str = (string)$max+1; 
                if($max < 9){
                    $no='0'.$str;
                }elseif($max < 99){
                    $no=$str;
                }
                $auto_id = 'slider-'.$no;
            }else{
                $auto_id = 'slider-01';
            }
            return $auto_id;
		}
	}

?>