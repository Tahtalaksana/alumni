<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Facebook\FacebookRequest;
use Facebook\FacebookSession;
class Site extends CI_Controller {

	public function __construct(){
		parent::__construct();
		/*$this->CI = get_instance();*/
		$this->load->model('m_slideshow');
		$this->load->model('m_testimonial');
		$this->load->model('m_journey');
		$this->load->library('form_validation');
	}

	public function index()
	{

		/*$this->CI = get_instance();
		$token = $this->CI->session->userdata('fb_token');
		$a = (new FacebookRequest($this->session,'get','/me'))->execute();
		$b = $request->getGraphObject()->asArray();
		var_dump($b);*/
		//die();
		
		if ($this->facebook->logged_in())
		{
			$user = $this->facebook->user();
			//var_dump($user);
			if ($user['code'] === 200)
			{
				$data=[
					'facebook_logged_in'=>true,
					'user_profile'=>$user['data']
				];
				$this->session->set_userdata($data);
			}

		}

		

		$data=[
			'slides'=>$this->m_slideshow->find_all(),
			'journey'=>$this->m_journey->recent_journey(),
			'testimonials'=>$this->m_testimonial->find_all(),
			'user_profile'=>$this->session->userdata('user_profile')
		];

		$this->load->view('site/index',$data);
	}

	public function ajax_journey_details(){
		if(!empty($this->input->post('id'))){
			$id = $this->input->post('id');
			$journey = $this->m_journey->find_one($id)->row();
			
			$json=[
				'content'	=> '<div class="post-wrapper">
									
									<!--Featured image -->
									<div class="view overlay hm-white-slight z-depth-2">
			                            <img src="'.base_url().'assets/img/journey/'.$journey->thumbnail.'" class="img-fluid" alt="">
			                            <a>
			                                <div class="mask waves-effect waves-light"></div>
			                            </a>
			                        </div>
			                        <div class="jumbotron m-1 text-xs-center">
	                            		<h2 class="h2-responsive">'.$journey->title.'</h2>
			                            <hr>
			                            <p>Written by '.$journey->author.', '.$journey->date.'</p>
                        			</div>
								    '.$journey->content.'
								</div>',
			];
			echo json_encode($json);
		}

		
	}


	public function submit(){
		$this->form_validation->set_rules('comment','Comment','required');
		if($this->form_validation->run()){
			$data=[
				'fb_id'		=> $this->session->userdata('user_profile')['id'],
				'comment'	=> set_value('comment'),
				'status'	=> 'waiting',
				'date'		=> date('Y-m-d H:i:s')
			];
			$this->m_testimonial->create($data);
			$this->session->set_flashdata('message','Your comment is awaiting moderation...');
			redirect($this);
		}else{
			redirect('');
		}
	}

	public function ajax_packet_details(){
		if(!empty($this->input->post('id'))){
			//$id = $this->input->post('id');
			//$journey = $this->m_journey->find_one($id)->row();
			$id = $this->input->post('id');
			if($id=='pk01'){
				$packet = ['title'=>'Silver Packet','content'=>'detail silver packet'];
			}elseif($id=='pk02'){
				$packet =['title'=>'Gold Packet','content'=>'detail gold packet'];
			}elseif($id=='pk03'){
				$packet =['title'=>'Platinum Packet','content'=>'detail platinum packet'];
			}

			$json=[
				'content'	=> '<h1>'.$packet['title'].'</h1>
								<p>'.$packet['content'].'</p>'
			];
			echo json_encode($json);
		}
	}
}
?>
								