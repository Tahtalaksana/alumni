<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Journey extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_journey');
	}

	public function index($slug=null)
	{
		
		$count= $this->m_journey->find_all()->num_rows();
 
		$config=[
					'base_url'=>base_url().'journey/index/',
					'total_rows'=>$count,
					'per_page'=>3,
		];

		$config=[	'base_url'			=> base_url().'journey/index/',
					'total_rows'		=> $count,
					'per_page'			=> 2,
					'full_tag_open'		=> "<ul class='pagination'>",
					'full_tag_close'	=> "</ul>",
					'num_tag_open'		=> "<li class='page-item'>",
					'num_tag_close'		=> "</li>" ,
					'cur_tag_open'		=> "<li class='page-item disabled'><li class='page-item active'><a href='#'>",
					'cur_tag_close'		=> "<span class='sr-only'></span></a></li>" ,
					'next_tag_open'		=> "<li class='page-item'>",
					'next_tagl_close'	=> "</li>",
					'prev_tag_open'		=> "<li class='page-item'>",
					'prev_tagl_close'	=> "</li>",
					'first_tag_open'	=> "<li class='page-item'>",
					'first_tagl_close'	=> "</li>",
					'last_tag_open'		=> "<li class='page-item'>",
					'last_tagl_close'	=> "</li>",
					'last_tagl_close'	=> "</li>",
		];

		$data=[
				'page_journey' => $this->m_journey->view($config['per_page'],$this->uri->segment('3')),
				'journeys'=>$this->m_journey->recent_journey()
		];
		$this->pagination->initialize($config);
	   	$this->load->view('journey/header');
	   	$this->load->view('journey/index',$data);
	   	$this->load->view('journey/footer');
	}

	public function read($slug){
		$data=[
				'journey' => $this->m_journey->detail($slug)->row(),
				'journeys'=>$this->m_journey->recent_journey()
		];
	   	$this->load->view('journey/header');
		$this->load->view('journey/detail',$data);
	   	$this->load->view('journey/footer');
	}

	public function search(){
		$data = $this->input->post('search-data');
		$data=[
				'result' => $this->m_journey->search($data),
				'journeys'=>$this->m_journey->recent_journey()
		];
		$this->load->view('journey/header');
		$this->load->view('journey/search_result',$data);
		$this->load->view('journey/footer');
	}

	
	
}
?>



