<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_slideshow');
		$this->load->model('m_journey');
	}

	public function index()
	{
		$slider = $this->m_slideshow->find_all();
		//var_dump($slider['result']);
		$data=[
			'content'=>'home/index',
			'slider_data'=>$slider['result'],
			'slider_count'=>$slider['row_count'],
		];

		$this->load->view('layout/wrapper',$data);
		
		/*$x = ['journey'	=> $this->m_journey->find_all()];
		$this->load->view('home/navigation');
		$this->load->view('home/slider',$data);
		$this->load->view('home/about');
		$this->load->view('home/journey',$x);
		$this->load->view('home/services');
		$this->load->view('home/packet');
		$this->load->view('home/testimonials');
		$this->load->view('home/footer');*/
	}
}
