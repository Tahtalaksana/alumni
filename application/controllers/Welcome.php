<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		
		if($this->session->userdata('facebook_logged_in') == true){
			redirect('welcome/profile');
		}
		
		
		if ($this->facebook->logged_in())
		{
			$user = $this->facebook->user();
			//var_dump($user);
			if ($user['code'] === 200)
			{
				$data=[
					'facebook_logged_in'=>true,
					'user_profile'=>$user['data']
				];
				$this->session->set_userdata($data);
				redirect('welcome/profile');
			}

		}
		
		 else {
	
	
			$contents['link'] = $this->facebook->login_url();
		
			$this->load->view('welcome_message',$contents);
		
	
		}
	}
	
	public function profile(){
		if($this->session->userdata('facebook_logged_in') != true){
			redirect('');
		}
		$contents['user_profile'] = $this->session->userdata('user_profile');
		$this->load->view('profile',$contents);
		
	}
	
	public function logout(){
		$this->session->unset_userdata('facebook_logged_in');
		session_destroy();
		redirect('');
		
	}
	
}
