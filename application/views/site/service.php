<section id="service">
    <div class="container">
        <h1 class="h1-responsive title wow fadeInDown">Our Services <small class="text-muted"> </small></h1>
        <div class="row">
            
            <div class="col-xs-12 col-sm-6 col-md-4 card-container">
                <!--journey-card-->
                <div class="card waves-effect waves-dark">
                    <div class="card-block">
                        <div class="service-icon">
                            <img src="<?=base_url()?>assets/img/services/services1.png" alt="">
                        </div>
                        <div class="service-body">
                            <p>Panduan Management Perjalanan</p>
                        </div>
                    </div>
                </div>
                <!--/.journey-card-->
            </div>      

            <div class="col-xs-12 col-sm-6 col-md-4 card-container">
                <!--journey-card-->
                <div class="card waves-effect waves-dark">
                    <div class="card-block">
                        <div class="service-icon">
                            <img src="<?=base_url()?>assets/img/services/services2.png" alt="">
                        </div>
                        <div class="service-body">
                            <p>Jadwal Yang Terencana</p>
                        </div>
                    </div>
                </div>
                <!--/.journey-card-->
            </div>      

            <div class="col-xs-12 col-sm-6 col-md-4 card-container">
                <!--journey-card-->
                <div class="card waves-effect waves-dark">
                    <div class="card-block">
                        <div class="service-icon">
                            <img src="<?=base_url()?>assets/img/services/services3.png" alt="">
                        </div>
                        <div class="service-body">
                            <p>Guide Profesional</p>
                        </div>
                    </div>
                </div>
                <!--/.journey-card-->
            </div>      

            <div class="col-xs-12 col-sm-6 col-md-4 card-container">
                <!--journey-card-->
                <div class="card waves-effect waves-dark">
                    <div class="card-block">
                        <div class="service-icon">
                            <img src="<?=base_url()?>assets/img/services/services4.png" alt="">
                        </div>
                        <div class="service-body">
                            <p>Porter Yang Handal</p>
                        </div>
                    </div>
                </div>
                <!--/.journey-card-->
            </div>      

            <div class="col-xs-12 col-sm-6 col-md-4 card-container">
                <!--journey-card-->
                <div class="card waves-effect waves-dark">
                    <div class="card-block">
                        <div class="service-icon">
                            <img src="<?=base_url()?>assets/img/services/services5.png" alt="">
                        </div>
                        <div class="service-body">
                            <p>Porter yang handal</p>
                        </div>
                    </div>
                </div>
                <!--/.journey-card-->
            </div>      

            <div class="col-xs-12 col-sm-6 col-md-4 card-container">
                <!--journey-card-->
                <div class="card waves-effect waves-dark">
                    <div class="card-block">
                        <div class="service-icon">
                            <img src="<?=base_url()?>assets/img/services/services6.png" alt="">
                        </div>
                        <div class="service-body">
                            <p>Memberikan Pengalaman Perjalanan terbaik</p>
                        </div>
                    </div>
                </div>
                <!--/.journey-card-->
            </div>      
        
        </div> 
    </div>
</section>