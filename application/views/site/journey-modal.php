<div class="post-wrapper">
	<!--Post data-->
	<h1 class="h1-responsive" >Title</h1>
	<h5 id="modal-desc">Written by <a href="">John Doe</a>, 30.04.2016</h5>
	<br>
	<!--Featured image -->
	<div class="view overlay hm-white-light z-depth-1-half">
    
    </div>
	<br>
    <!--Post excerpt-->
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</div>