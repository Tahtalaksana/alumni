<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Bromo Tour Travel</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="<?=base_url()?>assets/css/mdb.min.css" rel="stylesheet">

    <link href="<?=base_url()?>assets/css/animate.min.css" rel="stylesheet">
<!--     <link href="css/prettyPhoto.css" rel="stylesheet"> -->
    <link href="<?=base_url()?>assets/css/main.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/responsive.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet">

</head>


<body>
    
<!--Navbar-->
<nav id="my-navbar" class="navbar navbar-dark navbar-fixed-top ">

    <!-- Collapse button-->
    <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapseEx">
        <i class="fa fa-bars"></i>
    </button>

    <div class="container">

        <!--Collapse content-->
        <div class="collapse navbar-toggleable-xs" id="collapseEx">
            <!--Navbar Brand-->
            <a class="navbar-brand" href="#carousel-example-2" class="page-scroll">Bromo Travel</a>
            <!--Links-->
            <ul class="nav navbar-nav navbar-right"">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#carousel-example-2">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#about">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#recent-journey">Journey</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#packet">Packet</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#testimonial">Testimonial</a>
                </li>
            </ul>
            <!--Search form-->
            <!-- <form class="form-inline">
                <input class="form-control" type="text" placeholder="Search">
            </form> -->
        </div>
        <!--/.Collapse content-->
    </div>
</nav>
<!--/.Navbar-->
<script>
  $('.nav li').click(function(){
    alert('click menu');
    $('.nav li').removeClass('active');
    $(this).addClass('active');
  });
</script>


<!--Carousel Wrapper-->
<div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
    <!--Indicators-->
    <ol class="carousel-indicators">
        <?php for($i=0; $i<$slides->num_rows(); $i++): ?>
        <li data-target="#carousel-example-2" data-slide-to="<?=$i?>" class="<?php if($i==0)echo'active' ?>"></li>
        <?php endfor; ?> 
    </ol>
    <!--/.Indicators-->

    <!--Slides--> <div class="carousel-inner" role="listbox">
        <?php if($slides->num_rows()>0): ?>
            <?php $i=0; foreach ($slides->result() as $slide): ?>
                <div class="carousel-item <?php if($i==0)echo'active'?>">
                    <!--Mask color-->
                    <div class="view hm-black-slight">
                        <img src="<?=base_url().'assets/img/slides/'.$slide->img?>" class="img-fluid" alt="">
                        <div class="full-bg-img">
                        </div>
                    </div>
                    <!--Caption-->
                    <div class="carousel-caption">
                        <div class="animated fadeInDown">
                            <h3 class="h3-responsive"><?=$slide->caption?></h3>
                            <p><?=$slide->description?></p>
                        </div>
                    </div>
                    <!--Caption-->
                </div>
            <?php $i++; endforeach; ?>
        <?php endif; ?>

        <!--/Third slide-->
    </div>
    <!--/.Slides-->

    <!--Controls-->
    <a class="left carousel-control" href="#carousel-example-2" role="button" data-slide="prev">
        <span class="icon-prev" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-2" role="button" data-slide="next">
        <span class="icon-next" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->

<!--Main container-->
<div class="container">
    <!--Section: About-->
    <section id="about" class="about-us" >
        <div class="row">
            <h1 class="h1-responsive title-dark wow fadeInDown">About Us <small class="text-muted">make sure you know about us</small></h1>
                            
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="about-item animated fadeInDown" data-animated-duration="1000ms" data-animated-delay="600ms">
                        <i class="fa fa-car"></i>
                        <h4>Great Transport</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua..</p>
                    </div>
                </div>  

                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="about-item animated fadeInDown" data-animated-duration="1000ms" data-animated-delay="600ms">
                        <i class="fa fa-usd"></i>
                        <h4>Not Expensive</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua..</p>
                    </div>
                </div>  

                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="about-item animated fadeInDown" data-animated-duration="1000ms" data-animated-delay="600ms">
                        <i class="fa fa-cutlery"></i>
                        <h4>Delicious Food</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua..</p>
                    </div>
                </div>   
         </div>
    </section>
    <!--/Section: About--> 
</div>

<!--Section: Recent Journey-->
<section id="recent-journey" class="recent-journey">
    <div class="container">
        <div class="block-heading">
            <a href="<?= base_url('journey') ?>" class="btn btn-etc pull-right">View All</a>
        <h1 class="h1-responsive title-dark wow fadeInDown">Recent Journey <small class="text-muted">This is a record of our recent journey</small></h1>
        </div>

        <div class="row">

        <?php if($journey->num_rows()>0): ?>
            <?php foreach($journey->result() as $journey): ?>
                <div class="col-xs-12 col-sm-6 col-md-4 card-container">
                    <div class="card">
                        <a href="#" onclick="sendReques('journey',<?=$journey->id?>); return false;" class="recent-work"style="background-image:url(<?=base_url().'assets/img/journey/'.$journey->thumbnail?>)">
                            <span class="btn btn-o-white">View</span>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>

        </div>
        </div>
</section>
<!--/Section: Recent Journey-->



<div class="container">
    <!--Section: Packet-->
    <section id="packet">
        <h1 class="h1-responsive title-dark wow fadeInDown">Packet <small class="text-muted"> </small></h1>
        <div class="row">
        
        
            <!--First column-->
            <div class="col-md-4">

                <!--Pricing card-->
                <div class="card packet-card">
                    <!--Price-->
                    <div class="packet" style="background-color:#30cfc0">
                        <h1>Silver</h1>
                        <!-- <div class="version">
                            <h5>Basic</h5>
                        </div> -->
                    </div>
                    <!--/.Price-->

                    <!--Features-->
                    <div class="card-block">
                        <ul>
                            <li>
                                <p><i class="fa fa-check"></i> Feature name</p>
                            </li>
                            <li>
                                <p><i class="fa fa-check"></i> Feature name</p>
                            </li>
                            <li>
                                <p><i class="fa fa-times"></i> Feature name</p>
                            </li>
                            <li>
                                <p><i class="fa fa-times"></i> Feature name</p>
                            </li>
                            <li>
                                <p><i class="fa fa-times"></i> Feature name</p>
                            </li>
                        </ul>
                        <a href="#" onclick="sendReques('packet','pk01');return false;">
                            <button class="btn btn-default">Show Details</button>
                        </a>
                    </div>
                    <!--/.Features-->

                </div>
                <!--/.Pricing card-->
            </div>
            <!--/.First column-->
            <!--First column-->
            <div class="col-md-4">

                <!--Pricing card-->
                <div class="card packet-card">
                    <!--Price-->
                    <div class="packet" style="background-color:#1c2331">
                        <h1>Gold</h1>
                        <!-- <div class="version">
                            <h5>Basic</h5>
                        </div> -->
                    </div>
                    <!--/.Price-->

                    <!--Features-->
                    <div class="card-block">
                        <ul>
                            <li>
                                <p><i class="fa fa-check"></i> Feature name</p>
                            </li>
                            <li>
                                <p><i class="fa fa-check"></i> Feature name</p>
                            </li>
                            <li>
                                <p><i class="fa fa-times"></i> Feature name</p>
                            </li>
                            <li>
                                <p><i class="fa fa-check"></i> Feature name</p>
                            </li>
                            <li>
                                <p><i class="fa fa-times"></i> Feature name</p>
                            </li>
                        </ul>
                        <a href="#" onclick="sendReques('packet','pk02');return false;">
                            <button class="btn btn-etc">Show Details</button>
                        </a>
                    </div>
                    <!--/.Features-->

                </div>
                <!--/.Pricing card-->

            </div>
            <!--/.First column-->
            <!--First column-->
            <div class="col-md-4">

                <!--Pricing card-->
                <div class="card packet-card">
                    <!--Price-->
                    <div class="packet" style="background-color:#C00">
                        <h1>Platinum</h1>
                        <!-- <div class="version">
                            <h5>Basic</h5>
                        </div> -->
                    </div>
                    <!--/.Price-->

                    <!--Features-->
                    <div class="card-block">
                        <ul>
                            <li>
                                <p><i class="fa fa-check"></i> Feature name</p>
                            </li>
                            <li>
                                <p><i class="fa fa-check"></i> Feature name</p>
                            </li>
                            <li>
                                <p><i class="fa fa-check"></i> Feature name</p>
                            </li>
                            <li>
                                <p><i class="fa fa-check"></i> Feature name</p>
                            </li>
                            <li>
                                <p><i class="fa fa-check"></i> Feature name</p>
                            </li>
                        </ul>
                        <a href="#" onclick="sendReques('packet','pk03'); return false;"><button class="btn btn-danger">Show Details</button></a>
                        
                    </div>
                    <!--/.Features-->

                </div>
                <!--/.Pricing card-->

            </div>
            <!--/.First column-->


        </div>
    </section>
    <!--/Section: Packet-->
</div>
    <!-- section : testimonials -->
    <section id="testimonial" class="testimonial">
    <div class="container">
        <h1 class="h1-responsive title-dark wow fadeInDown">Testimonials <small class="text-muted">Lorem ipsum...</small>
        </h1>
        <div class="row">
            <?php if($testimonials->num_rows()>0): ?>
                <?php foreach($testimonials->result() as $testimonial): ?>
    <?php 
       $data = file_get_contents("https://graph.facebook.com/" . $testimonial->fb_id);
$fb_user = json_decode($data, true);

    ?>
    <?php var_dump($data) ?>
                    
                    <div class="col-md-4">
                        <img src="<?='https://graph.facebook.com/'.$testimonial->fb_id.'/picture?type=large'?>" class="img-circle img-responsive">
                
                                Name
                                <h4 class="text-center">
                                    <?=  $fb_user['name'] ?>
                                </h4>
                                <hr>
                                <p><i class="fa fa-quote-left"></i> <?= $testimonial->comment ?></p>
                        </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="col-md-4">
                    Testimonial not foud
                </div>
            <?php endif; ?>
        </div>
        
</div> <!-- /container -->
    </section>
<!-- /section : testimonials -->

<!--/Main container-->

<!--Footer-->
<footer class="page-footer center-on-small-only">
    <div class="container">
        <section>
        <!-- <h1 class="h1-responsive footer-title wow fadeInDown">Contact Us <small class="text-muted">Have question?</small></h1> -->
                <div class="row">
                    <!--First column-->
                    <div class="col-md-8">
                        
                        <div class="card-block">
                            <!--Header-->
                            <div class="text-xs-center">
                                <h3><i class="fa fa-envelope"></i> Write to us:</h3>
                            </div>
                            <!--Body-->
                            <br>

                            <!--Body-->
                            
                            <div class="md-form">
                                <?= form_open('site/submit'); ?>
                                    <textarea name="comment" type="text" id="form8" class="md-textarea" rows="5"></textarea>
                                    <label for="form8">Comment</label>
                                
                                    <div class="text-xs-center">
                                    <?php if($this->session->userdata('facebook_logged_in') == true): ?>
                                        <button type="submit" href="#" class="btn btn-default">Submit</button>
                                    <?php else: ?>
                                        <a href="<?= @$this->facebook->login_url() ?>" class="btn btn-fb"><i class="fa fa-facebook-square"></i> Connect to Facebook</a>
                                    <?php endif; ?>
                                    </div>
                                
                                <?= form_close(); ?>
                            </div>

                        </div>
                        <hr>
                    </div>
                    <!--/First column-->

                    <!--Second column-->
                    <div class="col-md-4">
                        <ul class="text-xs-center">
                            <li class="wow fadeInUp" data-wow-delay="0.2s"><a class="btn-floating btn-small mdb-color"><i class="fa fa-map-marker"></i></a>
                                <p>Probolinggo, Indonesia</p>
                            </li>

                            <li class="wow fadeInUp" data-wow-delay="0.3s"><a class="btn-floating btn-small mdb-color" data-toggle="modal" data-target="#contact-form"><i class="fa fa-phone"></i></a>
                                <p>+62 853-3618-0xxx</p>
                            </li>

                            <li class="wow fadeInUp" data-wow-delay="0.4s"><a class="btn-floating btn-small mdb-color" data-toggle="modal" data-target="#contact-form"><i class="fa fa-envelope"></i></a>
                                <p>contact@example.com</p>
                            </li>
                        </ul>
                    </div>
                    <!--/Second column-->
                </div>
            </section>
        <hr>

        <!--Call to action-->
        <div style="visibility: visible; animation-delay: 0.2s; animation-name: fadeIn;" class="social-section wow fadeIn" data-wow-delay="0.2s">
        <ul>
            <li><a href="https://www.facebook.com/mdbootstrap" class="btn btn-fb waves-effect waves-light"><i class="fa fa-facebook left"></i> Facebook</a></li>
            <li><a href="https://twitter.com/MDBootstrap" class="btn btn-tw waves-effect waves-light"><i class="fa fa-twitter left"></i> Twitter</a></li>
            <li><a href="https://plus.google.com/u/0/b/107863090883699620484/+Mdbootstrap/posts" class="btn btn-gplus waves-effect waves-light"><i class="fa fa-google-plus left"></i> Google +</a></li>
            <li><a href="http://mdbootstrap.com/forums/forum/support/" class="btn btn-comm waves-effect waves-light"><i class="fa fa-comments-o left"></i> Support</a></li>
        </ul>
    </div>
    </div>
        <!--/.Call to action-->

    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">
            © 2016 Copyright: <a href="#"> BromoTavel.com </a>

        </div>
    </div>
    <!--/.Copyright-->
</footer>
<!--/.Footer-->


<!-- Portfolio Modals -->
    <div class="my-modal-dialog modal fade" id="my-modal-dialog" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            dddd
                        </div>
                            <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- SCRIPTS -->


<script type="text/javascript" src="<?=base_url()?>/assets/js/jquery-2.2.3.min.js"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?=base_url()?>/assets/js/tether.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>/assets/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>/assets/js/mdb.min.js"></script>
<script>
    $(window).scroll(function() {
    if ($("#my-navbar.navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});


    $(function() {
        $('a.page-scroll').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 1500);
            event.preventDefault();
        });
    });

    function sendReques(modal_target,data){
        //alert("WORK "+data);
                var infoModal = $("#my-modal-dialog");
                infoModal.modal("show");
                var url;
                if(modal_target=='journey'){
                    url='<?= base_url()?>site/ajax_journey_details';
                }else if(modal_target=='packet'){
                    url='<?= base_url()?>site/ajax_packet_details'
                }
               // console.log('url '+url+data);

                $.ajax({
                    type    :'POST',
                    url     : url,
                    data    : {'id' : data,'<?=$this->security->get_csrf_token_name()?>':'<?=$this->security->get_csrf_hash()?>'},
                    dataType :'json',
                    beforeSend: function(){
                        infoModal.find('.modal-body').html("Please Wait...");
                        infoModal.modal('show');
                    },
                    success : function(data){
                        setTimeout(function(){
                            infoModal.find('.modal-body').html(data['content']);
                            infoModal.modal('show');
                        }, 0);
                    },
                    fail: function(){
                        setTimeout(function(){
                            infoModal.find('.modal-body').html('Failed to Load DAta');
                            infoModal.modal('show');
                        }, 0);
                    }
                });
                return false;
            }
</script>
<!-- <script src="http://connect.facebook.net/en_US/all.js"></script>
    <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '361574987563981',
      xfbml      : true,
      version    : 'v2.7'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script> -->

</body>

</html>