<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <!-- core CSS -->
    <link href="<?= base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/animate.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/main.css" rel="stylesheet">
    <link href="<?= base_url()?>assets/css/responsive.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?= base_url()?>assets/img/ico/favicon.ico">
</head><!--/head-->

<body>