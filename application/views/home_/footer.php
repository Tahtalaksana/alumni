
	

	<section id="footer-top">
		<div class="container">
			<div class="row">
			<!-- <div class="col-md-4">
				<h2 class="page-title">Have a question</h2>
				<p>Praesent lacinia dapibus, accumsan vesti bulum. Pellen tesque molestie mollis.</p>
				<p>Mauris nec ligula dui, fermentum nisl ut magna dolor, rhoncus wisi. In lacus sagittis luctus, nisl eros, sit amet tempor et, accumsan eget, pede. Suspendisse est. Ut rhoncus eu, pede. Vestibulum ante ipsum dolor lorem, iaculis mi. Pellentesque eu urna eget dolor. Duis luctus a, dolor. Duis ac sapien.
				</p>
				<p>Estibulum dapibus, maurimalesfames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus ultricies gravida vitae...</p>
			</div> -->

			<div class="col-md-4 ">
				<h2 class="page-title">Leave us a message</h2>
				<form action="">
					<div class="form-group">
						<input type="text" class="form-control flat" placeholder="Your Name">
					</div>
					<div class="form-group">
						<input type="text" class="form-control flat" placeholder="Your Mail">
					</div>
					<div class="form-group">
						<textarea class="form-control flat" name="" id="" rows="5" placeholder="Write Something" ></textarea>
					</div>
					<button type="submit" class="btn btn-danger form-control flat">Send</button>
				</form>
			</div>

			<div class="col-md-4">
				<h2 class="page-title">Contact Details</h2>
				<ul>
					<li class="address-sub"><i class="fa fa-home"></i> Office Address</li>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<li class="address-sub"><i class="fa fa-phone"></i> Phone</li>
					<p>Local: 1-800-123-hello <br>Mobile: 1-800-123-hello</p>
					<li class="address-sub"><i class="fa fa-envelope-o"></i> E-Mail</li>
					<p><a href="">bromotour@gmail.com</a> <br><a href="">bromotour@ymail.com</a></p>
				</ul>
			</div>

			<div class="col-md-4">
			<h2 class="page-title"> Map</h2>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d212646.11774361893!2d112.87319157234523!3d-7.972647547666488!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd637aaab794a41%3A0xada40d36ecd2a5dd!2sGn.+Bromo!5e0!3m2!1sid!2sid!4v1466696922790" width="100%" height="250px" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
		</div>
	</section>

	<footer id="footer" class="footer-bottom">
		<div class="container">
			Copyright &copy; <?php echo date("Y"); ?> - Bromo Tour And Travel <div class="pull-right">Powerred by Bootstrap 3</div>
		</div>	
	</footer>
	
</div>


<div class="fs-modal modal fade" id="myModal" class="modal fade" tabindex="-1" aria-hidden="true" role="dialog">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="close-modal" data-dismiss="modal">
      <div class="lr">
        <div class="rl"></div>
      </div>
    </div>
		<div class="container">
			<div class="row">
				<div class="col-lg-offset-2 col-lg-8">
   				<div class="modal-body">
   					<h1 class="page-title">Add Your Title Here</h1>
    				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</div>
				</div>
			</div>
		</div>
	</div>
</div>





<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- scrolling -->
<script src="<?= base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?= base_url(); ?>assets/js/scrolling-nav.js"></script>
<!-- animate -->
<script src="<?= base_url(); ?>assets/js/wow.min.js" type="text/javascript"></script>
<script>new WOW().init();</script>

<script>
	// Dropdown Menu Fade    
jQuery(document).ready(function(){
    $(".dropdown").hover(
        function() { $('.dropdown-menu', this).fadeIn("fast");
        },
        function() { $('.dropdown-menu', this).fadeOut("fast");
    });
});
</script>
</body>
</html>