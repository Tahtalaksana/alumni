<section id="our-service">
			<div class="container">
				<h1 class="page-title">Our Services <small>A little about what we do</small></h1>
				<div class="row">
					<?php $imgServices=["services1.png","services2.png","services3.png","services4.png","services5.png","services6.png"]; 
							$servicesName=['Panduan Management Perjalanan','Jadwal yang terencana','Guide Profesional','Porter yang handal','Keselamatan Terjamin','Memberikan Pengalaman Perjalanan terbaik']
					?>
  					<?php foreach ($imgServices as $i => $value) { ?>
  					<div class="col-md-4 col-sm-6">
							<div class="media">
						<div class="panel panel-default" style="min-height:200px">
							<div class="media-left">
								<img class="media-object" src="assets/img/services/<?php echo $imgServices[$i]; ?>" alt="logo">
							</div>
		  					<div class="media-body">
			  					<h4><?= $servicesName[$i]; ?></h4>
			  					<cite>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</cite>
		  					</div>
						</div>
						</div>
					</div>
  					<?php } ?>
				</div>
			</div>
		</section>