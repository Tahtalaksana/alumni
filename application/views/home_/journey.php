<section id="our-journey">
		<div class="container">
			<h1 class="page-title">Our Journey <small>Ini catatan perjalanan kami dari waktu ke waktu.</small></h1>
			<!-- <a href="" data-toggle="modal" data-target="#myModal">Click Me</a> -->
				<div class="row">
					<nav class="col-md-12">
			  <ul class="pagination">
			    <li>
			      <a href="#" aria-label="Previous">
			        <span aria-hidden="true">&laquo;</span>
			      </a>
			    </li>
			    <li><a href="#">1</a></li>
			    <li><a href="#">2</a></li>
			    <li><a href="#">3</a></li>
			    <li><a href="#">4</a></li>
			    <li><a href="#">5</a></li>
			    <li>
			      <a href="#" aria-label="Next">
			        <span aria-hidden="true">&raquo;</span>
			      </a>
			    </li>
			  </ul>
			</nav>
				</div>
			<!-- <header class="block-heading cleafix">
				<a href="#" class="btn btn-o btn-lg pull-right">View All</a>
				<div class="title-page">
					<p class="main-header">Our Latest Works </p>
				    <p class="sub-header">Take a look at some of our recent products</p>
			    </div>
			</header> -->
			
			<section class="block-body">
				<div class="row">
					
					

					<?php foreach($journey->result() as $data ):?>
						<div class="col-sm-4">
						<a href="#" class="recent-work" data-toggle="modal" data-target="#myModal" id="<?= $data->id ?>" style="background-image:url(assets/img/journey/<?= $data->thumbnail; ?>)">
							<span class="btn btn-o-white">show me</span>
						</a>
						</div>
					<?php endforeach; ?>
				</div>
			</section>
		</div>
	</section>
<!-- 
	<?php $imgPort=["journery-1.jpg","journery-2.jpg","journery-3.jpg","journery-4.jpg","journery-5.jpg","journery-6.jpg"] ?>
					<?php foreach ($imgPort as $i => $value) { ?>
						<div class="col-sm-4">
						<a href="#" class="recent-work" data-toggle="modal" data-target="#myModal" style="background-image:url(assets/img/journery/<?php echo $imgPort[$i]; ?>)">
							<span class="btn btn-o-white">show me</span>
						</a>
						</div>
					<?php } ?> -->