<section id="packet">
			<div class="container">
				<h1 class="page-title">Packet <small>kami menyediakan paket perjalanan.</small></h1>
				<div class="row">
					
					<?php $imgBlog=["bronze.png","platinum.png","gold.png"]; 
						$thumbPacket=['Bronze','Platinum','Gold'];
					?>

					<?php for($i=0; $i<3; $i++){ ?>
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail flat" style="background: transparent; border: 0;">
								<img src="assets/img/<?php echo $imgBlog[$i]; ?>" alt="...">
								<div class="caption">
									<h3><?= $thumbPacket[$i] ?></h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
									consequat.</p>
									<p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
								</div>
							</div>
						</div>
					<?php } ?>

				</div>
			</div>
		</section>