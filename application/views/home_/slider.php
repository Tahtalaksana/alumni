
	<section id="primary-carousel">
	    <div id="myCarousel" class="carousel slide " data-ride="carousel">
	      <!-- Indicators -->
	      <ol class="carousel-indicators">
			<?php for($i=0; $i<$slider_count; $i++){ ?>
	        <li data-target="#myCarousel" data-slide-to="<?= $i ?>" class="<?php if($i==0) echo'active';?>"></li>
	        <?php } ?>
	      </ol>
	    	
	  <!--     Wrapper for slides -->
	      <div class="carousel-inner" role="listbox">        
	    		
	    		<?php $i=0; foreach($slider_data as $slider){ ?>
	    			<div class="item <?php if($i==0) echo'active';?>">
			          <img src="<?=base_url().'assets/img/slider/'.$slider->img?>">
			          <div class="carousel-caption hidden-xs wow fadeInUp"  data-wow-duration="1000ms" data-wow-delay="600ms">
			            <h2><?=$slider->caption?></h2>
			            <p><?=$slider->description?></p>
			          </div>
			        </div>
	    		<?php $i++; } ?>
	   	  
	    	
	      </div><!-- .carousel-inner -->
	    	
	 <!--      Left and right controls -->
	      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
	        <span class="icon-prev" aria-hidden="true"></span>
	        <span class="sr-only">Previous</span>
	      </a>
	      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
	        <span class="icon-next" aria-hidden="true"></span>
	        <span class="sr-only">Next</span>
	      </a>
	    </div><!-- #myCarousel -->
	</section><!-- #home -->

	<!-- <section id="primary-section" class="content-block" style="height:500px">
		
	</section> -->


	

	

		

		






