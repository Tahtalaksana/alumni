<div id="page">
<section id="about">
		<div class="container">
			<h1 class="page-title">About Us <small>Make sure you know about us</small></h1>
            <div class="divide50"></div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <div class="aboutus-item">
                        <i class="aboutus-icon fa fa-car"></i>
                        <h4 class="aboutus-title">Great Transport</h4>
                        <p class="aboutus-desc">Kami Menyediakan transport .......</p>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <div class="aboutus-item">
                        <i class="aboutus-icon fa fa-usd"></i>
                        <h4 class="aboutus-title">Not Expensive</h4>
                        <p class="aboutus-desc">Harga yang kami berikan ........</p>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <div class="aboutus-item">
                        <i class="aboutus-icon fa fa-cutlery"></i>
                        <h4 class="aboutus-title">Delicious Food</h4>
                        <p class="aboutus-desc">Makanan yang kami sajikan kuliner Khas Indonesia .......</p>
                    </div>
                </div>
            </div>
		</div>
	</section>