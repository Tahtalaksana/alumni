<header id="header">
		<nav class="navbar navbar-inverse navbar-fixed-top">
		    <div class="container">
		        <div class="row">
		          <div class="col-md-12">
		          	<div class="navbar-header">
		            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
		              <span class="sr-only">Toggle navigation</span>
		              <span class="icon-bar"></span>
		              <span class="icon-bar"></span>
		              <span class="icon-bar"></span>
		            </button>
		            <a class="navbar-brand" href="#">Bromo Tour And Travel</a>
		          </div>

		          <div class="navbar-collapse collapse navbar-right">
		            <ul class="nav navbar-nav">
		              
						<li><a class="page-scroll" href="#header"><i class="fa fa-home"></i> Home</a></li>
						<li><a class="page-scroll" href="#about"><i class="fa fa-home"></i> About</a></li>
						<li><a class="page-scroll" href="#our-journey"><i class="fa fa-bookmark"></i> Our Journey</a></li>
						<li><a class="page-scroll" href="#our-service"><i class="fa fa-bookmark"></i> Our Services</a></li>
						<li><a class="page-scroll" href="#packet"><i class="fa fa-tasks"></i> Packet</a></li>
		            </ul>
		          </div>
		          </div>

		        </div><!-- row -->
		    </div>
		</nav>
	</header>