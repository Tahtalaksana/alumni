
    <div class="row">
        <div class="col-md-8">
          
            <?php if($page_journey->num_rows()>0): ?>
                <?php foreach ($page_journey->result() as $journey):?>

                    <div class="post-wrapper">
                                    
                                    <!--Featured image -->
                                    <div class="view overlay hm-white-slight z-depth-2">
                                        <img src="<?= base_url('assets/img/journey/'.$journey->thumbnail)?>" class="img-fluid" alt="">
                                        <a>
                                            <div class="mask waves-effect waves-light"></div>
                                        </a>
                                    </div>
                                    <div class="jumbotron m-1 text-xs-center">
                                        <h2 class="h2-responsive"><?=$journey->title ?></h2>
                                        <hr>
                                        <p>Written by <?=$journey->author?>, <?=$journey->date?></p>
                              
                                        <a href="<?=base_url('journey/read/'.$journey->slug)?>" class="btn btn-primary"></i> Read More</a>
                                    </div>

                                </div>
                   
                    <hr>
                <?php endforeach; ?>
            <?php endif; ?>
            
<nav class="text-xs-center">
            <?= $this->pagination->create_links()?>
            </nav>



           <!--  <nav class="text-xs-center"> -->
                       <!--  <ul class="pagination">
                           <li class="page-item disabled">
                               <a class="page-link" href="#" aria-label="Previous">
                                   <span aria-hidden="true">«</span>
                                   <span class="sr-only">Previous</span>
                               </a>
                           </li>
                           <li class="page-item active">
                               <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                           </li>
                           <li class="page-item"><a class="page-link" href="#">2</a></li>
                           <li class="page-item"><a class="page-link" href="#">3</a></li>
                           <li class="page-item"><a class="page-link" href="#">4</a></li>
                           <li class="page-item"><a class="page-link" href="#">5</a></li>
                           <li class="page-item">
                               <a class="page-link" href="#" aria-label="Next">
                                   <span aria-hidden="true">»</span>
                                   <span class="sr-only">Next</span>
                               </a>
                           </li>
                       </ul> -->
                   <!--  </nav> -->
        </div> <!-- col-md-8 -->

        <?php include('sidebar.php'); ?>

    </div>


