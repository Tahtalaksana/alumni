<div class="col-md-4">

    <div class="widget-wrapper">
    <h4>Recommend to read</h4>
    <br>
        <div class="horizontal-listing z-depth-1">
           <?php if($journeys->num_rows()>0): ?>
                    <?php foreach ($journeys->result() as $journey):?>
                    <!--First row-->
                    <div class="row">
                        <!--Image column-->
                        <div class="col-xs-4">
                            <div class="view overlay hm-white-slight">
                                <img src="<?= base_url('assets/img/journey/'.$journey->thumbnail)?>" class="img-fluid" alt="">
                                <a>
                                    <div class="mask waves-effect waves-light"></div>
                                </a>
                            </div>
                        </div>
                        <!--/.Image column-->

                        <!--Content column-->
                        <div class="col-xs-8">
                            <a href="<?=base_url('journey/read/'.$journey->slug)?>"><?= strlen($journey->title) >18? substr($journey->title,0,18).'..': $journey->title?></a>

                            <div class="card-data">
                                <ul>
                                    <li><i class="fa fa-clock-o"></i> <?=$journey->date?></li>
                                    <!-- <li><a><i class="fa fa-comments-o"></i>12</a></li> -->
                                </ul>
                            </div>
                        </div>
                        <!--/.Content column-->
                    </div>
                    <!--/.First row-->
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>

    <!-- <div class="widget-wrapper">
        <h4>Contact</h4>
        <br>
        <div class="card">
            <div class="card-block">
                <ul>
                    <li>Telpone : 082364723462</li>
                    <li>Main    : xxx@mail.com</li>
                </ul>
                <hr>
                <p>Find me on social media</p>
                <a target="_blank" href="#" class="btn btn-primary"><i class="fa fa-facebook-square"></i> Facebook</a>
                <a target="_blank" href="#" class="btn btn-default"><i class="fa fa-twitter-square"></i> Twitter</a>
            </div>
        </div>
    </div> -->
    
</div>