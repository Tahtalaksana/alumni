<div class="row">
    <div class="col-md-8">
        <div class="post-wrapper">
            <!--Featured image -->
            <div class="view overlay hm-white-slight z-depth-2">
                <img src="<?= base_url('assets/img/journey/'.$journey->thumbnail)?>" class="img-fluid" alt="">
                <a>
                    <div class="mask waves-effect waves-light"></div>
                </a>
            </div>
            <div class="jumbotron m-1 text-xs-center">
                <h2 class="h2-responsive"><?=$journey->title ?></h2>
                <hr>
                <p>Written by <?=$journey->author?>, <?=$journey->date?></p>
            </div>
            <?= $journey->content ?>
        </div>                   
        <hr>

        <!-- <div id="comments" class="">
            <div class="comments-list">
                <div class="section-heading text-center">
                    <h3>Comments <span class="label blue">62</span></h3>
                </div>
        
                <ul class="commentlist">
                    <li class="single-comment" id="li-comment-2013">
                        <div class="row">
                            <div class="col-xs-2">
                                <img alt="" src="http://1.gravatar.com/avatar/ac1ae1d4bd67c1ae806157200d087de7?s=100&amp;d=mm&amp;r=g" srcset="http://1.gravatar.com/avatar/ac1ae1d4bd67c1ae806157200d087de7?s=200&amp;d=mm&amp;r=g 2x" class="avatar img-fluid img-circle z-depth-1 photo" height="100" width="100">                    
                            </div>
        
                            <div class="col-xs-10">
                                <a class="user"><h4> <cite class="fn">Renato Bonário</cite></h4></a>
                                <div class="card-data">
                                    <ul> 
                                        <li><i class="fa fa-clock-o"></i> <a href="http://mdbootstrap.com/bootstrap-tutorial/#comment-2013"><time pubdate="" datetime="2016-01-29T18:26:03+00:00">
                                        January 29, 2016                            </time></a>                             </li>
                                    </ul>
                                </div>                                          
                                <p>Hi Michal, my name is Renato and i'm from Brazil. I would like to say thank you so much for share your knowledge! I searched something like this for weeks. Please, continue with the tutorials and this project. Thank you again!</p>
                            </div>
        
                        </div>
                        <div class="reply">
                        </div>.reply
                    </li>
                </ul>
                                .commentlist
            </div>
        
            <section>
                                                    <div class="reply-form">
                                                                <div id="respond" class="comment-respond">
                <h3 id="reply-title" class="comment-reply-title"></h3><h1 class="section-heading">Leave a reply </h1> <small><a rel="nofollow" id="cancel-comment-reply-link" href="/bootstrap-tutorial/#respond" style="display:none;">Cancel reply</a></small>                <form action="http://mdbootstrap.com/wp-comments-post.php" method="post" id="commentform" class="comment-form">
                     
                                                                    <div class="md-form">
                                                                        <i class="fa fa-pencil prefix"></i>
                                                                        <textarea id="comment" name="comment" type="text" class="md-textarea"></textarea>
                                                                        <label for="comment">Your message</label>
                                                                    </div> <div class="md-form">
                                                                        <i class="fa fa-user prefix"></i>
                                                                        <input type="text" id="author" name="author" class="form-control" value="" aria-required="true">
                                                                        <label for="name">Your name<span class="required">*</span></label>
                                                                    </div>
        <div class="md-form">
                                                                        <i class="fa fa-envelope prefix"></i>
                                                                        <input type="text" id="email" name="email" class="form-control" aria-required="true" value="">
                                                                        <label for="email">Your email<span class="required">*</span></label>
                                                                    </div>
        <div class="md-form">
                                                                        <i class="fa fa-home prefix"></i>
                                                                        <input type="text" id="url" name="url" class="form-control" value="">
                                                                        <label for="url">Your website</label>
                                                                    </div>
        <p class="form-submit"><i class="btn btn-primary waves-input-wrapper waves-effect waves-light" style="color:rgb(255, 255, 255);background:rgba(0, 0, 0, 0)"><input name="submit" type="submit" id="submit" class="waves-button-input" value="Post Comment" style="background-color:rgba(0,0,0,0);"></i> <input type="hidden" name="comment_post_ID" value="3508" id="comment_post_ID">
        <input type="hidden" name="comment_parent" id="comment_parent" value="0">
        </p><p style="display: none;"><input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="b4d4dbc63e"></p><p style="display: none;"></p>                <input type="hidden" id="ak_js" name="ak_js" value="1471168837888"></form>
                    </div>#respond
                                                            </div>
                                                </section>
                                                /.Leave a reply section
        </div> -->
       
    </div> <!-- col-md-8 -->

    <?php include('sidebar.php'); ?>

</div>
