
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Bromo Tour Travel</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="<?=base_url()?>assets/css/mdb.min.css" rel="stylesheet">

    <link href="<?=base_url()?>assets/css/animate.min.css" rel="stylesheet">
<!--     <link href="css/prettyPhoto.css" rel="stylesheet"> -->
    <link href="<?=base_url()?>assets/css/main.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/responsive.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet">

</head>


<body>
    
<!--Navbar-->
<nav class="navbar journey-bar navbar-dark navbar-fixed-top ">

    <!-- Collapse button-->
    <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#collapseEx">
        <i class="fa fa-bars"></i>
    </button>

    <div class="container">

        <!--Collapse content-->
        <div class="collapse navbar-toggleable-xs" id="collapseEx">
            <!--Navbar Brand-->
            <a class="navbar-brand" href="#">Bromo Travel</a>
            <!--Links-->
            <!-- <ul class="nav navbar-nav navbar-right"">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#carousel-example-2">Goto Main Site <span class="sr-only">(current)</span></a>
                </li>
                
            </ul> -->
            <!--Search form-->
            <?=form_open('journey/search/',['id'=>'searchForm','class'=>'form-inline'])?>
                <input class="form-control" id="searchJourney" name="search-data" type="text" placeholder="Search">
            <?=form_close()?>
        </div>
        <!--/.Collapse content-->

    </div>

</nav>
<!--/.Navbar-->
<div class="container">
    <section id="jumbotron">
        <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                        <h2 class="h2-responsive">Bromo Tour And Travel</h2>
                        <br>
                        <p>Ini Merupakan halaman yang menampilkan catatan perjalanan kami dari waktu ke waktu</p>
                        <hr>
                        <p>Kunjungi home page kami untuk mengetahui informasi lebih detail</p>
                        <a target="" href="<?=base_url()?>" type="button" class="btn btn-primary btn-stc waves-effect waves-light">Home Page</a>
                    </div>
        </div>
    </div>
    </section>
    